@extends('layouts.masterpage')

@section('estilo')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('contenido')
<form method="POST" action="{{ url('empleados') }}" class="form-horizontal">
  @csrf
<fieldset>

<!-- Form Name -->
<legend>Nuevo Empleado</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Nombre">Nombre</label>  
  <div class="col-md-5">
  <input id="textinput" name="nombre" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="apellido">Apellido</label>  
  <div class="col-md-5">
  <input id="textinput" name="apellido" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>


<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="cargo">Cargo</label>
  <div class="col-md-4">
    <select id="selectbasic" name="cargo" class="form-control">
       @foreach($cargos as $cargo)
        <option>{{ $cargo->Title }}</option>
       @endforeach
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="jefeD">Jefe directo</label>
  <div class="col-md-4">
    <select id="selectbasic" name="jefe" class="form-control">
    @foreach($jefes as $jefe)
        <option value="{{ $jefe->EmployeeId }}">{{$jefe->FirstName}}{{$jefe->LastName}} - {{$jefe->Title}}</option>
       @endforeach
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Fecha Nacimiento</label>  
  <div class="col-md-4">
  <input id="nacimiento" name="nacimiento" type="text"  class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Fecha Contratación</label>  
  <div class="col-md-4">
  <input id="contrato" name="contrato" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>



<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="direccion">Dirección</label>  
  <div class="col-md-5">
  <input id="textinput" name="dir" type="text" placeholder="" class="form-control input-md">
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="ciudad">Ciudad</label>  
  <div class="col-md-4">
  <input id="textinput" name="ciudad" type="text" placeholder="" class="form-control input-md">   
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email">Email</label>  
  <div class="col-md-5">
  <input id="email" name="email" type="text" placeholder="" class="form-control input-md"> 
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for=""></label>
  <div class="col-md-4">
    <button type="submit" class="btn btn-primary">Enviar</button>
  </div>
</div>

</fieldset>
</form>

@endsection

@section('jquery')
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
       //datepicker a algun control
      $(document).ready (function() {
        $( "#nacimiento" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
        $( "#contrato" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
       });
    </script>
@endsection